import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Tab1 extends StatefulWidget {
  @override
  _Tab1State createState() => _Tab1State();
}

class _Tab1State extends State<Tab1> with AutomaticKeepAliveClientMixin<Tab1> {
  @override
  void initState() {
    super.initState();
    print('initState Tab1');
  }

  @override
  Widget build(BuildContext context) {
    print('build Tab1');
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child: Container(
            color: Colors.red,
            margin:EdgeInsets.only(bottom: 300),
          child:
          WebView(
            initialUrl: "https://www.onemap.sg/amm/amm.html?mapStyle=Default&zoomLevel=15&marker=latLng:1.349649591,103.9513403!iwt:JTNDcCUzRU5UVUMlMjAoVGFtcGluZXMpJTNDJTJGcCUzRQ==!icon:fa-font!colour:red!rType:drive!rDest:1.31759343,103.7858406&marker=latLng:1.31759343,103.7858406!iwt:JTNDcCUzRUNvbGQlMjBTdG9yYWdlJTNDJTJGcCUzRQ==!icon:fa-bold!colour:darkgreen&popupWidth=200",
            javascriptMode: JavascriptMode.unrestricted,
          ),
        ),

      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
        backgroundColor: Colors.red[600],
      ),
    );

  }

  @override
  bool get wantKeepAlive => true;
}