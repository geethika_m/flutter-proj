// TODO Implement this library.
import 'dart:convert';

List<Employees> employeesFromJson(String str) => List<Employees>.from(json.decode(str).map((x) => Employees.fromJson(x)));

String employeesToJson(List<Employees> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Employees {
  Employees({
    this.plnAreaN,
  });

  String plnAreaN;

  factory Employees.fromJson(Map<String, dynamic> json) => Employees(
    plnAreaN: json["pln_area_n"],
  );

  Map<String, dynamic> toJson() => {
    "pln_area_n": plnAreaN,
  };
}
