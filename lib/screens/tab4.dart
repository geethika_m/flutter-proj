import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'Employees.dart';


class Tab4 extends StatefulWidget {
  @override
  _Tab4State createState() => _Tab4State();
}

class _Tab4State extends State<Tab4> with AutomaticKeepAliveClientMixin<Tab4> {
  @override
  void initState() {
    super.initState();
    print('initState Tab4');
  }

  void buttonTapped(){
    print('Inside button');
    static const String url = 'https://developers.onemap.sg/privateapi/popapi/getPlanningareaNames?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjYzNTgsInVzZXJfaWQiOjYzNTgsImVtYWlsIjoiYW5uaWUucG5nQGJldmVhdC5jb20iLCJmb3JldmVyIjpmYWxzZSwiaXNzIjoiaHR0cDpcL1wvb20yLmRmZS5vbmVtYXAuc2dcL2FwaVwvdjJcL3VzZXJcL3Nlc3Npb24iLCJpYXQiOjE2MDA5OTU3MjIsImV4cCI6MTYwMTQyNzcyMiwibmJmIjoxNjAwOTk1NzIyLCJqdGkiOiJkNDg4ZTFhMTNjNjFhNjAxYzYxZjU4ZWQ5OTNjNTljMiJ9.QrdlhH1HxvujQikb39AsY95hovJnK10ZRGPk6XJJtt0';

    static Future<List<Employees>> getEmployees() async {
      try {
        final response = await http.get(url);
        if (response.statusCode == 200) {
          final List<Employees> listEmployees = employeesFromJson(response.body);
          return listEmployees;
        } else {
          return List<Employees> ();
        }
      } catch(e) {
        return List<Employees> ();
      }
    }

  }

  @override
  Widget build(BuildContext context) {
    print('build Tab4');
    return Scaffold(
      appBar: AppBar(
        title: Text('Records'),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child: FlatButton(
          onPressed:
           buttonTapped,
          child: Text('Fetch JSON data'),
          color: Colors.redAccent,
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}